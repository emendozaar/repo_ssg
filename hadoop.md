##¿Qué es Hadoop?

**Hadoop** es un *framework de software* que soporta aplicaciones distribuidas bajo una licencia libre.
Permite a las aplicaciones trabajar con miles de nodos y petabytes de datos.
