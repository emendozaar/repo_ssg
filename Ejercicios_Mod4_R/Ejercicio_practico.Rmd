---
title: "Ejercicio Práctico"
author: "Cristian Flores & Erick  Mendoza Arias"
date: "29/9/2018"
output:
  pdf_document: default
  html_document: default
---

```{r echo=FALSE, message=FALSE, warning=FALSE}
library(tidyverse)
##Importamos los datos
setwd(getwd())

datos<-read.csv("Base_marginacion_AGEB_00-10.csv")

library(xtable)
options(xtable.floating = FALSE)
options(xtable.timestamp = "")

```

# 1. Entendamos un poco la base de datos

La base de datos que utilizaremos proviene de CONAPO y muestra la intensidad de la marginación e incluye los indicadores socioeconómicos calculados de forma quinquenal para cada unidad geográfica. Los índices de marginación se calculan desde 1990 a la fecha y hacen referencia a entidades federativas, municipios, localidades y áreas geoestadísticas básicas (AGEB) urbanas.

Cabe resaltar que la estimación del índice de marginación se basa en la técnica de componentes principales, por lo que no es posible realizar comparación en el tiempo.

La base de datos tiene la siguiente estructura:

|Variable | Descripción |
|-------- | ----------- |
|CVE_ENT | Clave de entidad federativa |
|NOM_ENT | Nombre de entidad |
|CVE_MUN | Clave de municipio |
|NOM_MUN | Nombre de municipio |
|CVE_LOC | Clave de localidad |
|NOM_LOC | Nombre de localidad |
|CVE_AGEB | Clave AGEB |
|Cve_MNZ | Clave de manzana |
|POB_TOT | Población total | 
|P6A14NAE | % de población de 6 a 14 años que no asiste a la escuela |
|P1SYMSSC | % de población de 15 años o más sin secundaria completa |
|PSDSS | % de población sin derechohabiencia a los servicios de salud |
|HFM15A49 |% de hijos fallecidos para las mujeres entre 15 y 49 años |
|VSADV| % de viviendas particulares sin agua entubada dentro de la vivienda |
|VSDRPFS | % de viviendas particulares sin  drenaje conectado a la red pública séptica |
|VSECA | % de viviendas particulares sin excusado con conexión de agua |
|VPTIERRA | % de viviendas particulares con piso de tierra |
|VHACINA | % de viviendas particulares con algún hacinamiento |
|VSREFRI | % de viviendas sin refrigerador |
|P15YMSPP | % de la población de 15 años y más sin instrucción postprimaria |
|VSDRE | % de viviendas particulares sin drenaje |
|VTMLNP | % de viviendas particulares con techos de materiales ligeros, naturales o precarios |
|POI2SM | % de la población ocupada con ingresos de hasta 2 salarios mínimos |
|M12A17HV | % de mujeres entre 12 y 17 años de edad con al menos un hijo nacido vivo |
|IMU | Índice de marginación urbana |
|GMU | Grado de marginación urbana |
|A—O | Anio |

Validamos que nuestros datos están en forma de tabla, que no hay 2 o más headers y que los nombres de las columnas estén bien. Además, observamos los primeros registros.

```{r echo=FALSE, results=TRUE}
head(datos)
```

La base de datos tiene 25 variables, contamos el número de registros

```{r echo=TRUE,results=TRUE,eval=TRUE}
datos %>% count()
```

Los tipos de datos son:

```{r echo=FALSE, results=TRUE}
sapply(datos, class)
```

Los datos están bien


|Variable | Descripción | Valores | Tipo dato |
|-------- | ----------- | ------- | --------- |
|CVE_ENT | Clave de entidad federativa | 1, 2 | Integer |
|NOM_ENT | Nombre de entidad | Aguasc | Factor |
|CVE_MUN | Clave de municipio | 1001, 1002 | Integer |
|NOM_MUN | Nombre de municipio | Aguasc | Factor |
|CVE_LOC | Clave de localidad | 10010001, 10010002 | Integer |
|NOM_LOC | Nombre de localidad | Aguasc | Factor |
|CVE_AGEB | Clave AGEB | 10910 | Integer |
|Cve_MNZ | Clave de manzana | 1234567| Integer |
|POB_TOT | Población total | 500 | Integer |
|P6A14NAE | % de población de 6 a 14 años que no asiste a la escuela | 14.5 | Integer |
|P1SYMSSC | % de población de 15 años o más sin secundaria completa | 14.5 | Integer |
|PSDSS | % de población sin derechohabiencia a los servicios de salud | 14.5 | Integer |
|HFM15A49 |% de hijos fallecidos para las mujeres entre 15 y 49 años | 14.5 | Integer |
|VSADV| % de viviendas particulares sin agua entubada dentro de la vivienda | 14.5 | Integer |
|VSDRPFS | % de viviendas particulares sin  drenaje conectado a la red pública séptica | 14.5 | Integer |
|VSECA | % de viviendas particulares sin excusado con conexión de agua | 14.5 | Integer |
|VPTIERRA | % de viviendas particulares con piso de tierra | 14.5 | Integer |
|VHACINA | % de viviendas particulares con algún hacinamiento | 14.5 | Integer |
|VSREFRI | % de viviendas sin refrigerador | 14.5 | Integer |
|P15YMSPP | % de la población de 15 años y más sin instrucción postprimaria | 14.5 | Integer |
|VSDRE | % de viviendas particulares sin drenaje | 14.5 | Integer |
|VTMLNP | % de viviendas particulares con techos de materiales ligeros, naturales o precarios | 14.5 | Integer |
|POI2SM | % de la población ocupada con ingresos de hasta 2 salarios mínimos | 14.5 | Integer |
|M12A17HV | % de mujeres entre 12 y 17 años de edad con al menos un hijo nacido vivo | 14.5 | Integer |
|IMU | Índice de marginación urbana | 5.2 | Integer |
|GMU | Grado de marginación urbana | Bajo, Muy alto | Factor |
|A—O | Anio | 2000 | Integer |

Análisis de numéricos

```{r echo=TRUE,eval= TRUE, results=TRUE}
numericos<-datos %>% select(P6A14NAE, P15YMSSC, PSDSS, HFM15A49,  VSADV, VSDRPFS,
                            VSECA,VPTIERRA,VHACINA,VSREFRI,P15YMSPP,VSDRE,VTMLNP,POI2SM,
                            POI2SM,M12A17HV,IMU
                            )
summary(numericos)

```

Histograma y boxplot

```{r echo=TRUE,eval=TRUE, results=TRUE}
ggplot(data = datos) +
  geom_histogram(mapping = aes(x = IMU), binwidth =0.75)+coord_cartesian(xlim=c(0:9))+theme_bw()
```
El histograma anterior muestra la districión del IMU por localidad.

```{r echo=TRUE,eval=TRUE, results=TRUE}
ggplot(data = datos) +
  geom_boxplot(mapping = aes(x = reorder(CVE_ENT, IMU, FUN = median), y = IMU))+theme_bw()
```
Los boxplot muestran la distribución del IMU por entidad federativa, están ordenados de forma ascendente por la mediana.

## Error Cuadrático Medio (ECM)

* Usaremos la media de la variable objetivo (***"Índice de Marginalidad Urbana"***) como su estimador para posteriormente calcular el ECM. 

```{r echo=TRUE}
Y = datos$IMU
summary(Y)
```
Guardamos la media de esta variable en un vector llamado *Y_pred* de tamaño igual al número de observaciones que la base de datos.
```{r echo=TRUE}
Y_pred = rep(mean(Y),dim(datos)[1])
```
$$\text{Error Cuadrático Medio}\\\\ECM=\displaystyle\sum_{t=1}^{n} \frac{(y_t - \hat{y}_t)^2}{n}\\\\$$

```{r echo=TRUE}
# Función para calcular el ECM
ECM <- function(y,y_pred){
  error <- sum((y-y_pred)^2)/length(y)
  return(error)
}
# Mandamos a llamar a la función ECM
ECM(y=Y,y_pred=Y_pred)
```

## BONUS - Otra medida de error

Alternativamente calculamos otra medida de error llamada MAPE 

$$\text{Mean Absolute Percentage Error}\\\\MAPE =\displaystyle \frac{\displaystyle\sum_{t=1}^{n} \frac{|A_t - F_t|}{|A_t|}}{n}$$

```{r echo=TRUE}
# Función para calcular el MAPE
MAPE <- function(y,y_pred){
  error <- sum(abs(y-y_pred)/abs(y))/length(y)
  return(error)
}
# Mandamos a llamar a la función ECM
MAPE(y=Y,y_pred=Y_pred)

```

##Ejercicio. 

Define las variables y unidades observacionales del dataset con el que estás trabajando.

|Variable | Unidad Observacional |
|-------- | -------------------- |
|Cve_AGEB | Clave de la AGEB por localidad |

