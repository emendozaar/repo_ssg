---
title: "Ejercicio Práctico"
author: "Erick Ulises Mendoza Arias"
date: "29/9/2018"
output: pdf_document
---

```{r echo=FALSE, message=FALSE, warning=FALSE}
library(tidyverse)
##Importamos los datos
##setwd("~/Desktop/Diplomado Data Science/2.Módulo IV/CitiBanamex_clase3")

datos<-read.csv("IAIM_Entidad_2010.csv")

```

# 1. Entendamos un poco la base de datos

La base de datos que utilizaremos proviene de CONAPO y es el índice absoluto de intensidad migratoria es una medida que resume las características migratorias de los hogares mexicanos en términos de remesas, migrantes residentes en Estados Unidos, migrantes circulares y migrantes de retorno.

La base de datos tiene la siguiente estructura:

|Variable | Descripción |
|-------- | ----------- |
|ENT | Clave de entidad federativa |
|NOM_ENT | Nombre de entidad |
|TOT_VIV | Total de viviendad |
|VIV_REM | % Viviendas que reciben remesas |
|VIV_EMIG | % Viviendas con emigrantes a EU |
|VIV_CIRC | % Viviendas con migrantes circulares |
|VIV_RET | % Viviendas con migrantes con retorno |
|IAIM | Índice de intensidad migratoria |
|GAIM | Grado de intensidad migratoria |
|POS_NAL | Lugar que ocupa en posición nacional |

Validamos que nuestros datos están en forma de tabla, que no hay 2 o más headers y que los nombres de las columnas estén bien. Además, observamos los primeros registros.

```{r echo=FALSE, results=TRUE}
head(datos)
```

La base de datos tiene 10 variables, contamos el número de registros

```{r echo=TRUE,results=TRUE,eval=TRUE}
datos %>% count()
```

Los tipos de datos son:

```{r echo=FALSE, results=TRUE}
sapply(datos, class)
```

Los datos están bien, solo debemos modificar ENT y POS_NAL que son factores

```{r echo=TRUE, results=TRUE, eval=TRUE}
cols <- c("ENT", "POS_NAL")
datos[cols] <- lapply(datos[cols], factor)
```

|Variable | Descripción | Valores | Tipo Dato |
|-------- | ----------- | ------- | --------- |
|ENT | Clave de entidad federativa | 1,2,3.. | Factor | 
|NOM_ENT | Nombre de entidad | Aguascalientes | Factor |
|TOT_VIV | Total de viviendad | 121,2312 | Numérica |
|VIV_REM | % Viviendas que reciben remesas | 1232 | Numérica |
|VIV_EMIG | % Viviendas con emigrantes a EU | 1232 | Numérica |
|VIV_CIRC | % Viviendas con migrantes circulares | 1232 | Numérica |
|VIV_RET | % Viviendas con migrantes con retorno | 1232 | Numérica |
|IAIM | Índice de intensidad migratoria | 1.2 | Numérica |
|GAIM | Grado de intensidad migratoria | Alto | Factor |
|POS_NAL | Lugar que ocupa en posición nacional | 1,2,3 | Factor |

Análisis de numéricos

```{r echo=TRUE, results=TRUE}
numericos<-datos %>% select(TOT_VIV, VIV_REM, VIV_EMIG, VIV_CIRC,  VIV_RET, IAIM)
 summary(numericos)
```

Histograma y Boxplot 

```{r echo=TRUE, results=TRUE}
ggplot(data = datos) + 
  geom_histogram(mapping = aes(x=TOT_VIV))+theme_bw()
```

El histograma anterior muestra la distribución del total de viviendas de las 32 entidades federativas del país.

```{r echo=TRUE, results=TRUE}
ggplot(data = datos, mapping = aes(x=GAIM, y=IAIM)) +
  geom_boxplot()+theme_bw()
```

Las gráficas de caja y brazo muestran el índice de absoluto de intensidad migratoria por el grado de intensidad migratoria, por obvias razones, a mayo rango mayor es el índice de intensidad.