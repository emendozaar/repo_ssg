#SAS (Statistical Analysis System) 

Es un sistema de programas para el análisis de datos. Consiste de un conjunto de módulos capaces de entregar resultados de diferentes procesos como regresión, análisis 
de varianza, estadística básica, distribución de frecuencias, procedimientos multivariados y muchos mas.
