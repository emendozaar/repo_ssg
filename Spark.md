# Spark

Apache Spark combina un sistema de computación distribuida a través de clusters de ordenadores con una manera sencilla y elegante de escribir programas. Fue creado en la Universidad de Berkeley en 
California y es considerado el primer software de código abierto que hace la programación distribuida realmente accesible a los científicos de datos.
